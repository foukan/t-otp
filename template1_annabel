<p><span lang="EN-US" style="font-size: 12pt;">Dear {0} {1},</span></p>
<p><span lang="EN-US" style="font-size: 12pt;">We have the great pleasure to inform you that you are admitted to the <strong>{4_EN}.</strong></span></p>
<p>&nbsp;</p>
<p class="MsoNormal"><span style="font-size: 12pt;">You can <span style="text-decoration: underline;"><strong>download your admission certificate</strong></span> via <strong>the Result Master Recruitment 2024 interface {2}.</strong></span></p>
<p><span style="font-size: 12pt;">Identifier: <em>{3}</em></span></p>
<p>&nbsp;</p>
<p class="MsoNormal"><span style="font-size: 12pt;">The registration procedure will be sent to you by email later (in July).</span></p>
<p class="MsoNormal"><span style="font-size: 12pt;">The academic year will start on <strong>Monday September 02, 2024.</strong></span></p>
<p class="MsoNormal"><span style="font-size: 12pt;">If you wish, you will also have the opportunity to take an <strong>intensive week of French as a foreign language from August 26 to 30, 2024</strong> (information to be announced).</span></p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal"><span style="text-decoration: underline; font-size: 12pt;"><strong>As visa procedures take a long time, we recommend that you start as early as possible!</strong></span></p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal"><span style="font-size: 12pt; color: rgb(224, 62, 45);"><strong>Please read the information below carefully.</strong></span></p>
<p class="MsoNormal">&nbsp;</p>
<ul>
<li class="MsoNormal" style="font-weight: bold; color: rgb(53, 152, 219); font-size: 12pt;"><span style="text-decoration: underline; font-size: 12pt;"><span style="color: rgb(53, 152, 219); text-decoration: underline;"><strong>CONFIRMATION OF PRESENCE (before May 31)</strong></span></span></li>
</ul>
<p><span style="font-size: 12pt;">Please confirm or not your intention to participate in the <span lang="EN-US" style="line-height: 107%; font-family: Arial, sans-serif; color: black; background: white;">{4_EN} by visiting the <em>Result Master Recruitment 2024 interface</em>.</span></span></p>
<p>&nbsp;</p>
<ul>
<li style="color: rgb(53, 152, 219); font-weight: bold; font-size: 12pt;"><span style="text-decoration: underline; font-size: 12pt;"><strong><span lang="EN-US" style="line-height: 107%; font-family: Arial, sans-serif; color: rgb(53, 152, 219); background: white; text-decoration: underline;">ACCOMODATION</span></strong></span></li>
</ul>
<p><span lang="EN-US" style="font-size: 12pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background: white;"><span style="text-decoration: underline;">University residence Crous Grenoble</span> : <em>information available in April</em></span></p>
<p><span lang="EN-US" style="font-size: 12pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background: white;">You want to be housed in a University Residence for the beginning of the academic year in September? An agreement betweek Grenoble INP and the Crous of Grenoble allow students of foreign nationality registering for at least one academic year to be eligible for the allocation of accomodaton in a student residence.</span></p>
<p><span lang="EN-US" style="font-size: 12pt; line-height: 107%; font-family: Arial, sans-serif; color: black; background: white;"><a href="https://www.grenoble-inp.fr/en/student-life/housing-in-university-residence">https://www.grenoble-inp.fr/en/student-life/housing-in-university-residence</a></span></p>
<p class="MsoNormal"><span style="font-size: 12pt;"><strong><span style="color: rgb(0, 0, 0);">But first of all, please informed us (by indicating it in the Result Master Recruitment 2024 interface) that you wish to be housed in a University Residence.</span></strong></span></p>
<p class="MsoNormal"><span style="font-size: 12pt; color: rgb(0, 0, 0);">Other accomodation available:</span></p>
<p><span style="font-size: 12pt;"><span style="color: rgb(0, 0, 0);">- </span><span style="text-decoration: underline;"><span style="color: rgb(0, 0, 0); text-decoration: underline;">Houille Blanche Residence (private residence - for Grenoble INP students)</span></span></span></p>
<p class="MsoNormal"><span style="font-size: 12pt; color: rgb(0, 0, 0);">information available : <a href="https://www.rhbgrenoble.com/">https://www.rhbgrenoble.com/</a></span></p>
<p><span style="font-size: 12pt;"><span style="color: rgb(0, 0, 0);">- </span><span style="text-decoration: underline;"><span style="color: rgb(0, 0, 0); text-decoration: underline;">General information about accomodation:</span></span></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);"><a href="https://international.univ-grenoble-alpes.fr/getting-organized/accommodation/looking-for-accommodation/tips-for-finding-accommodation/tips-for-finding-accommodation-783848.kjsp?RH=1610025605865">https://international.univ-grenoble-alpes.fr/getting-organized/accommodation/looking-for-accommodation/tips-for-finding-accommodation/tips-for-finding-accommodation-783848.kjsp?RH=1610025605865</a></span></p>
<p>&nbsp;</p>
<ul>
<li style="font-size: 12pt;"><span style="text-decoration: underline; font-size: 12pt;"><span style="color: rgb(53, 152, 219); text-decoration: underline;"><strong>TUITION FEES</strong></span></span></li>
</ul>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">The compulsory registration fees are as follows:</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">- <strong>Non EU students: 5000&euro; / year + 100&euro; - Student and Campus life contribution</strong> (<em>rates 2023/2024</em>)</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">- <strong>European students: 1473&euro; / year + 100&euro; - Student and Campus life contribution</strong> (<em>rates 2023/2024</em>)</span></p>
<p><span style="color: rgb(126, 140, 141); font-size: 12pt;"><em>The institution applies the rules incurring the payement of differentiated fees by non EU students. The full amount of the owed registration fees is not necessrily to be paid at the start of the academic year: an instalment plan of payment may be scheduled. In addition, the admitted students will receive information about the various scholarships available to Grenoble INP-UGA students.</em></span></p>
<p>&nbsp;</p>
<ul>
<li style="font-weight: bold; color: rgb(53, 152, 219); font-size: 12pt;"><span style="text-decoration: underline; font-size: 12pt;"><span style="color: rgb(53, 152, 219); text-decoration: underline;"><strong>SCOLARSHIPS</strong></span></span></li>
</ul>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">Please answer the questions on the <em>Result Master Recruitment 2024 interface</em>.</span></p>
<p><span style="font-size: 12pt;"><strong><span style="color: rgb(0, 0, 0);">- </span></strong><span style="text-decoration: underline;"><strong><span style="color: rgb(0, 0, 0); text-decoration: underline;">Graduate school - Futurprod:</span></strong></span></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">This program is dedicated to high-potential students with an outstanding academic record, wishing to pursue a <span style="text-decoration: underline;">Reserarch Oriented curriculum.</span></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">Below ar the links for more information:</span></p>
<p><span style="font-size: 12pt;"><a href="https://genie-industriel.grenoble-inp.fr/en/studies/the-graduate-school">https://genie-industriel.grenoble-inp.fr/en/studies/the-graduate-school</a></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);"><a href="https://www.univ-grenoble-alpes.fr/education/graduate-school/futur-prod-transition-to-sustainable-industrial-production-systems-1024438.kjsp">https://www.univ-grenoble-alpes.fr/education/graduate-school/futur-prod-transition-to-sustainable-industrial-production-systems-1024438.kjsp</a></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">You can submit your application by clicking on the following link: <a href="https://enquetes.univ-grenoble-alpes.fr/SurveyServer/s/dgdform-gradschool/GS/candidature12.htm">https://enquetes.univ-grenoble-alpes.fr/SurveyServer/s/dgdform-gradschool/GS/candidature12.htm</a></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">Application deadline: May 31, 2024</span></p>
<p><span style="color: rgb(0, 0, 0); font-size: 12pt;"><strong><a style="color: rgb(0, 0, 0);" href="https://www.univ-grenoble-alpes.fr/universite/ambition-et-strategie/l-initiative-d-excellence-idex/les-appels-a-projets/call-for-applications-idex-master-scholarships-for-the-2023-2024-academic-year-1184934.kjsp"><span style="text-decoration: underline;">- </span>Idex Sholarship</a></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">&rarr; You can apply for both scholarships, but only one schoarship can be awarded to you: it cannot be combined.</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">&rarr; You should be aware that very few scholarships are available each year.</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">&rarr; Scholarships will be assigned when the recruitment process is completed (about early June).</span></p>
<p>&nbsp;</p>
<ul>
<li style="color: rgb(53, 152, 219); font-size: 12pt;"><span style="text-decoration: underline; font-size: 12pt;"><span style="color: rgb(53, 152, 219); text-decoration: underline;"><strong>ADMINISTRATIVE PROCEDURES AND PRACTICAL LIFE IN FRANCE</strong></span></span></li>
</ul>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">In the meantime, we invite you to consult the links below to help you with the various administrative procedures.</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);"><a href="https://genie-industriel.grenoble-inp.fr/fr/formation/prepare-your-stay-1">https://genie-industriel.grenoble-inp.fr/fr/formation/prepare-your-stay-1</a></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);"><a href="https://international.univ-grenoble-alpes.fr/getting-organized/">https://international.univ-grenoble-alpes.fr/getting-organized/</a></span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);"><a href="https://www.grenoble-inp.fr/en/academics/incoming-scholarships">https://www.grenoble-inp.fr/en/academics/incoming-scholarships</a></span></p>
<p>&nbsp;</p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">Do not hesitate to contact us if you encounter any difficulties or if you need additional information,</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">Best regards,</span></p>
<p><span style="font-size: 12pt; color: rgb(0, 0, 0);">The Master Team</span></p>
<p><span style="font-size: 12pt;"><a href="mailto:genie-industriel.scolarite-master@grenoble-inp.fr">genie-industriel.scolarite-master@grenoble-inp.fr</a></span></p>
<p><span style="font-size: 12pt;"><img src="https://gricad-gitlab.univ-grenoble-alpes.fr/foukan/t-otp/-/raw/main/logo.png" alt="" width="400" height="131"></span></p>
<p>&nbsp;</p>
